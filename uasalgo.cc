#include <iostream>
using namespace std;

int main() {

    const int jumlah_mahasiswa = 5;
    double nilai[jumlah_mahasiswa], quiz[jumlah_mahasiswa], absen[jumlah_mahasiswa], uts[jumlah_mahasiswa], uas[jumlah_mahasiswa], tugas[jumlah_mahasiswa];
    char Huruf_Mutu[jumlah_mahasiswa];

 
    for (int i = 0; i < jumlah_mahasiswa; ++i) {
        cout << "Mahasiswa " << i + 1 << endl;
        cout << "Masukkan nilai Quiz: ";
        cin >> quiz[i];
        cout << "Masukkan nilai Absen: ";
        cin >> absen[i];
        cout << "Masukkan nilai UTS: ";
        cin >> uts[i];
        cout << "Masukkan nilai UAS: ";
        cin >> uas[i];
        cout << "Masukkan nilai Tugas: ";
        cin >> tugas[i];

     
        nilai[i] = ((0.1 * absen[i]) + (0.2 * tugas[i]) + (0.3 * quiz[i]) + (0.4 * uts[i]) + (0.5 * uas[i])) / 2;
    }


    for (int i = 0; i < jumlah_mahasiswa; ++i) {
        if (nilai[i] > 85 && nilai[i] <= 100)
            Huruf_Mutu[i] = 'A';
        else if (nilai[i] > 70 && nilai[i] <= 85)
            Huruf_Mutu[i] = 'B';
        else if (nilai[i] > 55 && nilai[i] <= 70)
            Huruf_Mutu[i] = 'C';
        else if (nilai[i] > 40 && nilai[i] <= 55)
            Huruf_Mutu[i] = 'D';
        else if (nilai[i] >= 0 && nilai[i] <= 40)
            Huruf_Mutu[i] = 'E';

        cout << "Mahasiswa " << i + 1 << ":" << endl;
        cout << "Nilai rata-rata: " << nilai[i] << endl;
        cout << "Huruf Mutu: " << Huruf_Mutu[i] << endl;
    }

    return 0;
}
